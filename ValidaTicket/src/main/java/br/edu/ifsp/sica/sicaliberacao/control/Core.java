package br.edu.ifsp.sica.sicaliberacao.control;

import java.awt.EventQueue;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import br.edu.ifsp.sica.sicaliberacao.view.Principal;

public class Core {

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    JFrame frame = new JFrame("SICA - Valida Ticket");
                    Principal window = new Principal();
                    ImageIcon img = new ImageIcon(getClass().getResource("/img/Food-Dome.png"));
                    frame.setIconImage(img.getImage());
                    frame.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
                    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    frame.getContentPane().add(window);
                    frame.pack();
                    frame.setIconImage(img.getImage());
                    frame.setLocationRelativeTo(null);
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
