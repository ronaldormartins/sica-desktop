package br.edu.ifsp.sica.business.dao;

import java.util.HashMap;
import java.util.Map;

public class DBAccess {
    public static Map<String, String> getDBAccess(String address){
        HashMap<String, String> dbAccess = new HashMap<>();
        dbAccess.put("url", "jdbc:postgresql://"+address+":5432/sica");
        dbAccess.put("nome", "sica");
        dbAccess.put("senha", "sicapass");
        return dbAccess;
    }
}
