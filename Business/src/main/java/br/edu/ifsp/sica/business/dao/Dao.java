package br.edu.ifsp.sica.business.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import javax.swing.JOptionPane;

public abstract class Dao {

    private Connection con;
    private final  String addressDataBase;
    
    public Dao(String addressDataBase){
        this.addressDataBase = addressDataBase;
    }
    
    protected void conectar() {
        try {
            Map<String, String> dbAccess = DBAccess.getDBAccess(this.addressDataBase);
            con = ConectionFactory.conexao(dbAccess.get("url"), dbAccess.get("nome"), dbAccess.get("senha"), ConectionFactory.POSTGRESQL);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            imprimeErro("Erro ao carregar o driver", e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
            imprimeErro("Erro ao conectar", e.getMessage());
        }
    }

    protected void imprimeErro(String msg, String msgErro) {
        JOptionPane.showMessageDialog(null, msg, "Erro crítico", 0);
        System.err.println(msg);
        System.out.println(msgErro);
        System.exit(0);
    }

    public Connection getConn() {
        return this.con;
    }
}
