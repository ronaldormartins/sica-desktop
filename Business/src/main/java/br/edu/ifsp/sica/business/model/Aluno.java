package br.edu.ifsp.sica.business.model;

public class Aluno {

    private int id;
    private String prontuario;
    private String nome;
    private String sobrenome; 
    private String cursoDescricao;
    private int cursoId;
    private int ano;
    private String senha;
    private boolean assistenciaEstudantil;
    private int creditoRefeicao;
    private boolean ativo;

    public String getProntuario() {
        return prontuario;
    }

    public void setProntuario(String prontuario) {
        this.prontuario = prontuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public void setCursoDescricao(String cursoDescricao) {
        this.cursoDescricao = cursoDescricao;
    }

    public String getCursoDescricao() {
        return cursoDescricao;
    }

    public void setCursoId(int cursoId) {
        this.cursoId = cursoId;
    }

    public int getCursoId() {
        return cursoId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getAno() {
        return ano;
    }

    public void setAssistenciaEstudantil(boolean assistenciaEstudantil) {
        this.assistenciaEstudantil = assistenciaEstudantil;
    }

    public boolean isAssistenciaEstudantil() {
        return assistenciaEstudantil;
    }

    public void setCreditoRefeicao(int creditoRefeicao) {
        this.creditoRefeicao = creditoRefeicao;
    }

    public int getCreditoRefeicao() {
        return creditoRefeicao;
    }
    
    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public boolean isAtivo() {
        return ativo;
    }
}
