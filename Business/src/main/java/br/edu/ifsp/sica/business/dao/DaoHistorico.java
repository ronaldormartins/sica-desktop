package br.edu.ifsp.sica.business.dao;

import br.edu.ifsp.sica.business.model.Aluno;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class DaoHistorico extends Dao {
    
    public DaoHistorico(String addressDataBase) {
        super(addressDataBase);
    }
    
    public boolean IncrementaUnidade(Aluno aluno, int unidade) {
        boolean retorno = false;
        
        conectar();

        try {
            //Atualiza credito
            PreparedStatement ps = 
                    getConn().prepareStatement(
                            " update aluno_aluno set credito_refeicao = (credito_refeicao + ?)" + 
                            " where id = ?");
            ps.setLong(1, unidade);
            ps.setInt(2, aluno.getId());
            int linhasAfetadas = ps.executeUpdate();
            if (linhasAfetadas > 0){
                //Cria histórico
                ps = getConn().prepareStatement(
                        " insert into creditos_historico"+
                        " (data_criacao, justificativa, unidades, aluno_id, usuario_id)" +
                        " values"+
                        " (CURRENT_TIMESTAMP, 'Crédito consumido', ?, ?, (select id from auth_user where username = 'admin'))",
                        Statement.RETURN_GENERATED_KEYS);
                
                ps.setLong(1, unidade);
                ps.setInt(2, aluno.getId());
                linhasAfetadas = ps.executeUpdate();
                
                retorno = linhasAfetadas > 0;
                ps.close();
            }else{
                retorno = false;
                ps.close();
            }
             

        } catch (SQLException e) {
            imprimeErro("Erro ao incrementar crédito", e.getMessage());
            return false;
        }
        
        return retorno;
    }
    
    
    public boolean resetaUnidade(Aluno aluno) {
        boolean retorno = false;
        
        conectar();

        try {
            //Atualiza credito
            PreparedStatement ps = 
                    getConn().prepareStatement(
                            " update aluno_aluno set credito_refeicao = 0" + 
                            " where id = ?");
            ps.setInt(1, aluno.getId());
            int linhasAfetadas = ps.executeUpdate();
            if (linhasAfetadas > 0){
                //Cria histórico
                ps = getConn().prepareStatement(
                        " insert into creditos_historico"+
                        " (data_criacao, justificativa, unidades, aluno_id, usuario_id)" +
                        " values"+
                        " (CURRENT_TIMESTAMP, 'Resetado', 0, ?, (select id from auth_user where username = 'admin'))",
                        Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, aluno.getId());
                linhasAfetadas = ps.executeUpdate();
                
                retorno = linhasAfetadas > 0;
                ps.close();
            }else{
                retorno = false;
                ps.close();
            }
             

        } catch (SQLException e) {
            imprimeErro("Erro ao resetar crédito", e.getMessage());
            return false;
        }
        
        return retorno;
    }
    
}
