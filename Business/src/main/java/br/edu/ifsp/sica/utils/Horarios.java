package br.edu.ifsp.sica.utils;

import java.time.LocalTime;
import java.util.Calendar;

public class Horarios {

    private static boolean isBetweenHour(int hourBase, int minuteBasem, int hourBegin, int minuteBegin, int hourEnd, int minuteEnd) {
        LocalTime currentHour = LocalTime.of(hourBase, minuteBasem);

        // Define os horários de início e fim desejados
        LocalTime begin = LocalTime.of(hourBegin, minuteBegin);
        LocalTime end = LocalTime.of(hourEnd, minuteEnd);
        
        if (currentHour.isAfter(begin) && currentHour.isBefore(end)) {
            return true;
        } else {
            return false;
        }
    }
    
    public static boolean isHorarioGerarTicket(Calendar data){
        int hourBase = data.get(Calendar.HOUR_OF_DAY);
        int minuteBase = data.get(Calendar.MINUTE);
        return Horarios.isBetweenHour(hourBase, minuteBase, 6, 39, 12, 01);
    }
    
    public static boolean isHorarioConsumo(int hourBase, int minuteBase){
        return Horarios.isBetweenHour(hourBase, minuteBase, 11, 59, 13, 21);
    }
    
    public static String getDataProximaRefeicao(Calendar data) {
        int dia, mes, ano;
        if (data.get(Calendar.DAY_OF_WEEK) == 6) {
            dia = data.get(Calendar.DAY_OF_MONTH) + 3;
        } else {
            dia = data.get(Calendar.DAY_OF_MONTH) + 1;
        }
        mes = data.get(Calendar.MONTH) + 1;
        ano = data.get(Calendar.YEAR);

        String dataProxRefeicao = (("" + dia).length() == 1 ? "0" + dia : dia) + "/"
                + (("" + mes).length() == 1 ? "0" + mes : mes) + "/"
                + ano;
        
        return dataProxRefeicao;
    }
}
