package br.edu.ifsp.sica.business.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import br.edu.ifsp.sica.business.model.Aluno;
import br.edu.ifsp.sica.business.model.Horario;
import java.sql.PreparedStatement;
import java.sql.Statement;

public class DaoHorario extends Dao {

    public DaoHorario(String addressDataBase) {
        super(addressDataBase);
    }

    public Horario buscar(Aluno aluno) {
        conectar();
        Horario horario = null;

        try {
            PreparedStatement ps = getConn().prepareStatement("select * from horario_horario where aluno_id = ?");
            ps.setInt(1, aluno.getId());
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                horario = new Horario();
                horario.setAluno(aluno);
                horario.setId(rs.getInt("id"));
                horario.setSeg(rs.getBoolean("seg"));
                horario.setTer(rs.getBoolean("ter"));
                horario.setQua(rs.getBoolean("qua"));
                horario.setQui(rs.getBoolean("qui"));
                horario.setSex(rs.getBoolean("sex"));
            }
            rs.close();
            return horario;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar horario", e.getMessage());
            return null;
        }
    }
    
    public boolean remove(Aluno aluno) {
        conectar();
        boolean retorno = false;

        try {
            PreparedStatement ps = getConn().prepareStatement("delete from horario_horario where aluno_id = ?");
            ps.setInt(1, aluno.getId());
            retorno = ps.execute();
            ps.close();
            return retorno;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar horario", e.getMessage());
            return retorno;
        }
    }
    
    public boolean insert(Aluno aluno, Horario horario) {
        conectar();
        boolean retorno = false;

        try {
            PreparedStatement ps = getConn().prepareStatement("insert into horario_horario "
                    + " (seg, ter, qua, qui, sex, aluno_id)"
                    + " values (?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            ps.setBoolean(1, horario.isSeg());
            ps.setBoolean(2, horario.isTer());
            ps.setBoolean(3, horario.isQua());
            ps.setBoolean(4, horario.isQui());
            ps.setBoolean(5, horario.isSex());
            ps.setInt(6, aluno.getId());
            
            int linhasAfetadas = ps.executeUpdate();
            
            if (linhasAfetadas > 0) {
                ResultSet generatedKeys = ps.getGeneratedKeys();
                if (generatedKeys.next()) {
                    int idGerado = generatedKeys.getInt(1);
                    horario.setId(idGerado); // Definindo o ID gerado no objeto Aluno
                    generatedKeys.close();
                }
            }
            ps.close();
            return retorno;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar horario", e.getMessage());
            return retorno;
        }
    }
}
