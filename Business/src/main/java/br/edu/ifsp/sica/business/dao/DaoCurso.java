package br.edu.ifsp.sica.business.dao;

import br.edu.ifsp.sica.business.model.Cota;
import java.sql.ResultSet;
import java.sql.SQLException;
import br.edu.ifsp.sica.business.model.Curso;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DaoCurso extends Dao {

    public DaoCurso(String addressDataBase) {
        super(addressDataBase);
    }

    public List<Curso> buscaTodos() {
        conectar();
        List<Curso> cursos = new ArrayList<>();
        
        try {
            PreparedStatement ps = getConn().prepareStatement(
                    " select cs.id as idCurso, cs.curso as cursoCurso, cs.ativo as ativoCurso,"+
                    "        ct.id as idCota, nome as nomeCota, quantidade as quantidadeCota" + 
                    " from curso_curso cs "+
                    " left join cota_cota ct on cs.cota_id = ct.id");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Curso curso = new Curso();
                curso.setId(rs.getInt(1));
                curso.setCurso(rs.getString(2));
                curso.setAtivo(rs.getBoolean(3));
                
                int idCota = rs.getInt("idCota");
                if (!rs.wasNull()) { // Se idCota não for NULL, então existe uma cota vinculada
                    Cota cota = new Cota();
                    cota.setId(idCota);
                    cota.setNome(rs.getString("nomeCota"));
                    cota.setQuantidade(rs.getInt("quantidadeCota"));
                    curso.setCota(cota);
                } else {
                    // Se idCota for NULL, não vincule uma cota ao curso
                    curso.setCota(null);
                }
                
                cursos.add(curso);
            }
            rs.close();
            return cursos;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar aluno", e.getMessage());
            return null;
        }
    }
    
    public Curso buscaPorId(int id) {
        conectar();
        Curso curso = null;
        
        try {
            PreparedStatement ps = getConn().prepareStatement(
                    " select cs.id as idCurso, cs.curso as cursoCurso, cs.ativo as ativoCurso,"+
                    "        ct.id as idCota, nome as nomeCota, quantidade as quantidadeCota" + 
                    " from curso_curso cs "+
                    " left join cota_cota ct on cs.cota_id = ct.id"+
                    " where cs.id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                curso = new Curso();
                curso.setId(rs.getInt(1));
                curso.setCurso(rs.getString(2));
                curso.setAtivo(rs.getBoolean(3));
                
                int idCota = rs.getInt("idCota");
                if (!rs.wasNull()) { // Se idCota não for NULL, então existe uma cota vinculada
                    Cota cota = new Cota();
                    cota.setId(idCota);
                    cota.setNome(rs.getString("nomeCota"));
                    cota.setQuantidade(rs.getInt("quantidadeCota"));
                    curso.setCota(cota);
                } else {
                    // Se idCota for NULL, não vincule uma cota ao curso
                    curso.setCota(null);
                }
            }
            rs.close();
            return curso;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar aluno", e.getMessage());
            return null;
        }
    }
    
    public Curso buscaPorNome(String nome) {
        conectar();
        Curso curso = null;
        
        try {
            PreparedStatement ps = getConn().prepareStatement(
                    " select cs.id as idCurso, cs.curso as cursoCurso, cs.ativo as ativoCurso,"+
                    "        ct.id as idCota, nome as nomeCota, quantidade as quantidadeCota" + 
                    " from curso_curso cs "+
                    " left join cota_cota ct on cs.cota_id = ct.id"+
                    " where cs.curso = ?");
            ps.setString(1, nome);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                curso = new Curso();
                curso.setId(rs.getInt(1));
                curso.setCurso(rs.getString(2));
                curso.setAtivo(rs.getBoolean(3));
                
                int idCota = rs.getInt("idCota");
                if (!rs.wasNull()) { // Se idCota não for NULL, então existe uma cota vinculada
                    Cota cota = new Cota();
                    cota.setId(idCota);
                    cota.setNome(rs.getString("nomeCota"));
                    cota.setQuantidade(rs.getInt("quantidadeCota"));
                    curso.setCota(cota);
                } else {
                    // Se idCota for NULL, não vincule uma cota ao curso
                    curso.setCota(null);
                }
            }
            rs.close();
            return curso;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar curso", e.getMessage());
            return null;
        }
    }
    
    public boolean insert(Curso curso) {
        conectar();
        boolean retorno = false;
        
        try {
            PreparedStatement ps = getConn().prepareStatement(
                    " insert into curso_curso"
                    + " (curso, ativo, cota_id)"
                    + " values"
                    + " (?, ?, ?) ", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, curso.getCurso());
            ps.setBoolean(2, curso.isAtivo());
            if (curso.getCota() != null)
                ps.setInt(3, curso.getCota().getId());
            else
                ps.setNull(3, java.sql.Types.INTEGER);
            
            int linhasAfetadas = ps.executeUpdate();
            
             if (linhasAfetadas > 0) {
                ResultSet generatedKeys = ps.getGeneratedKeys();
                if (generatedKeys.next()) {
                    int idGerado = generatedKeys.getInt(1);
                    curso.setId(idGerado);
                    generatedKeys.close();
                }
            }
            ps.close();
            return retorno;
        } catch (SQLException e) {
            imprimeErro("Erro ao inserir curso", e.getMessage());
            return retorno;
        }
    }  
}
