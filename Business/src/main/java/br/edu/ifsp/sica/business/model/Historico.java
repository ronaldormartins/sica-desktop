package br.edu.ifsp.sica.business.model;

import java.sql.Date;

public class Historico {
    private int id;
    private Date dataCriacao;
    private String justificativa;
    private int usuarioId;
    private Aluno aluno;
    private int unidades;

    public void setId(int id) {
        this.id = id;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }

    public void setUnidades(int unidades) {
        this.unidades = unidades;
    }

    public int getId() {
        return id;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public String getJustificativa() {
        return justificativa;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public Aluno getAluno() {
        return aluno;
    }

    public int getUnidades() {
        return unidades;
    }
}
