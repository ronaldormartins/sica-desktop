package br.edu.ifsp.sica.business.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ConectionFactory {

    public static final int POSTGRESQL = 0;
    public static final String PostgreSQLDriver = "org.postgresql.Driver";
    public static Connection connection;
    
    public static Connection conexao(String url, String nome, String senha, int banco) throws ClassNotFoundException, SQLException {
        switch (banco) {
            case POSTGRESQL:
                Class.forName(PostgreSQLDriver);
                break;
        }
        if (connection==null || connection.isClosed())
            connection = DriverManager.getConnection(url, nome, senha);
        try{
            PreparedStatement ps = connection.prepareStatement("select 1");
            ps.executeQuery();
            ps.close();
        }catch(SQLException ex){
            connection = DriverManager.getConnection(url, nome, senha);
        }
        return connection;
    }
}
