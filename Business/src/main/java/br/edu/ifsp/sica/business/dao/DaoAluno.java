package br.edu.ifsp.sica.business.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import br.edu.ifsp.sica.business.model.Aluno;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;

public class DaoAluno extends Dao {

    public DaoAluno(String addressDataBase) {
        super(addressDataBase);
    }

    public Aluno buscar(String prontuario) {
        conectar();
        Aluno aluno = null;
        
        try {
            PreparedStatement ps = getConn().prepareStatement(
                    " select a.*, c.curso as curso_nome, c.id as curso_id"+
                    " from aluno_aluno a, curso_curso c"+
                    " where cast(a.curso as integer) = c.id and prontuario LIKE ? ");
            ps.setString(1, prontuario.toLowerCase());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                aluno = new Aluno();
                aluno.setProntuario(rs.getString("prontuario"));
                aluno.setNome(rs.getString("nome"));
                aluno.setSobrenome(rs.getString("sobrenome"));
                aluno.setId(Integer.parseInt(rs.getString("id")));
                aluno.setSenha(rs.getString("senha"));
                aluno.setCursoDescricao(rs.getString("curso_nome"));
                aluno.setCursoId(rs.getInt("curso_id"));
                aluno.setAno(rs.getInt("ano"));
                aluno.setAtivo(rs.getBoolean("ativo"));
                aluno.setCreditoRefeicao(rs.getInt("credito_refeicao"));
                aluno.setAssistenciaEstudantil(rs.getBoolean("assistencia_estudantil"));
            }
            rs.close();
            return aluno;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar aluno", e.getMessage());
            return null;
        }
    }
    
    public boolean remove(Aluno aluno) {
        conectar();
        boolean retorno = false;
        
        try {
            PreparedStatement ps = getConn().prepareStatement(" delete from aluno_aluno where id = ? ");
            ps.setInt(1, aluno.getId());
            retorno = ps.execute();
            ps.close();
            return retorno;
        } catch (SQLException e) {
            imprimeErro("Erro ao remover aluno", e.getMessage());
            return retorno;
        }
    }  
    
    public boolean insert(Aluno aluno) {
        conectar();
        boolean retorno = false;
        
        try {
            PreparedStatement ps = getConn().prepareStatement(" insert into aluno_aluno"
                    + " (prontuario, nome, sobrenome, curso, ano, ativo, senha, assistencia_estudantil, data_criacao, credito_refeicao)"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?, current_timestamp, ?) ", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, aluno.getProntuario().toLowerCase());
            ps.setString(2, aluno.getNome());
            ps.setString(3, aluno.getSobrenome());
            ps.setString(4, aluno.getCursoId()+"");
            ps.setInt(5, aluno.getAno());
            ps.setBoolean(6, aluno.isAtivo());
            ps.setString(7,aluno.getSenha()==null?"":aluno.getSenha());
            ps.setBoolean(8, aluno.isAssistenciaEstudantil());
            ps.setInt(9, aluno.getCreditoRefeicao());
            int linhasAfetadas = ps.executeUpdate();
            
             if (linhasAfetadas > 0) {
                ResultSet generatedKeys = ps.getGeneratedKeys();
                if (generatedKeys.next()) {
                    int idGerado = generatedKeys.getInt(1);
                    aluno.setId(idGerado);
                    generatedKeys.close();
                }
            }
            ps.close();
            return retorno;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar aluno", e.getMessage());
            return retorno;
        }
    }  
}
