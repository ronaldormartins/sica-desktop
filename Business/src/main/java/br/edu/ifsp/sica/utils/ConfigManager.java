package br.edu.ifsp.sica.utils;

import java.io.*;
import java.util.Properties;

public class ConfigManager {
    
    public static final String ADDRESS_DATABASE = "AddressDataBase";
    
    private static final String CONFIG_FILE_PATH = "config.properties";

    public static void setConfigValue(String key, String value) {
        Properties properties = loadProperties();
        properties.setProperty(key, value);
        saveProperties(properties);
    }

    public static String getConfigValue(String key) {
        Properties properties = loadProperties();
        return properties.getProperty(key);
    }

    private static Properties loadProperties() {
        Properties properties = new Properties();

        try (InputStream inputStream = new FileInputStream(CONFIG_FILE_PATH)) {
            properties.load(inputStream);
        } catch (IOException e) {
            // Se o arquivo de configuração não existe, cria um novo vazio
            System.out.println("Arquivo de configuração não encontrado. Criando novo arquivo.");
        }

        return properties;
    }

    private static void saveProperties(Properties properties) {
        try (OutputStream outputStream = new FileOutputStream(CONFIG_FILE_PATH)) {
            properties.store(outputStream, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static String getPathFile(){
        File configFile = new File(CONFIG_FILE_PATH);
        return configFile.getAbsolutePath();
    }
}

