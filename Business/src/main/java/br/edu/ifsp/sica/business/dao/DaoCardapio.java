package br.edu.ifsp.sica.business.dao;

import br.edu.ifsp.sica.business.model.Cardapio;
import br.edu.ifsp.sica.utils.Horarios;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

public class DaoCardapio extends Dao {

    public DaoCardapio(String addressDataBase) {
        super(addressDataBase);
    }

    public Cardapio buscarCardapioDia(Calendar dataBase) {
        conectar();

        Cardapio card = new Cardapio();
        try {
            String dataFormatada = dataBase.get(Calendar.DAY_OF_MONTH) + "/" + (dataBase.get(Calendar.MONTH) + 1) + "/" + dataBase.get(Calendar.YEAR);
            
            PreparedStatement ps = getConn().prepareStatement("select * from core_cardapio where data = to_date( ?, 'dd/mm/yyyy')");
            ps.setString(1, dataFormatada);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                card.setPrincipal(" " + rs.getString(2));
                card.setSalada(" " + rs.getString(3));
                card.setSobremesa(" " + rs.getString(4));
                card.setSuco(" " + rs.getString(5));
            }
            rs.close();
            return card;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar cardapio", e.getMessage());
            card.setPrincipal("Erro");
            card.setSalada("Erro");
            card.setSobremesa("Erro");
            card.setSuco("Erro");
            return card;
        }
    }
    
    public Cardapio buscarCardapioProximoTicket(Calendar dataBase) {
        conectar();

        Cardapio card = new Cardapio();
        try {
            String data = Horarios.getDataProximaRefeicao(dataBase);
            PreparedStatement ps = getConn().prepareStatement("Select * from core_cardapio where data = to_date( ?, 'dd/mm/yyyy')");
            ps.setString(1, data);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                card.setPrincipal(" " + rs.getString(2));
                card.setSalada(" " + rs.getString(3));
                card.setSobremesa(" " + rs.getString(4));
                card.setSuco(" " + rs.getString(5));
            }
            rs.close();
            return card;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar cardapio", e.getMessage());
            card.setPrincipal("Erro");
            card.setSalada("Erro");
            card.setSobremesa("Erro");
            card.setSuco("Erro");
            return card;
        }
    }

}
