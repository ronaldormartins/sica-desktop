package br.edu.ifsp.sica.business.services;

import br.edu.ifsp.sica.business.dao.DaoAluno;
import br.edu.ifsp.sica.business.dao.DaoCurso;
import br.edu.ifsp.sica.business.dao.DaoHistorico;
import br.edu.ifsp.sica.business.dao.DaoHorario;
import br.edu.ifsp.sica.business.dao.DaoRefeicao;
import br.edu.ifsp.sica.business.dao.DaoTicket;
import br.edu.ifsp.sica.business.model.Aluno;
import br.edu.ifsp.sica.business.model.Cota;
import br.edu.ifsp.sica.business.model.Curso;
import br.edu.ifsp.sica.business.model.Horario;
import br.edu.ifsp.sica.utils.Horarios;
import java.util.Calendar;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import org.apache.commons.lang3.StringUtils;

public class TicketService {

    private final DaoAluno daoAluno;
    private final DaoTicket daoTicket;
    private final DaoHorario daoHorario;
    private final DaoRefeicao daoRefeicao;
    private final DaoCurso daoCurso;
    private final DaoHistorico daoHistorico;
    
    public static class TicketResultGerar {
        private STATUS_GERAR_TICKET status;
        private Aluno aluno;

        public TicketResultGerar(STATUS_GERAR_TICKET status, Aluno aluno) {
            this.status = status;
            this.aluno = aluno;
        }

        // Getters e setters
        public STATUS_GERAR_TICKET getStatus() {
            return status;
        }

        public void setStatus(STATUS_GERAR_TICKET status) {
            this.status = status;
        }

        public Aluno getAluno() {
            return aluno;
        }

        public void setAluno(Aluno aluno) {
            this.aluno = aluno;
        }
    }
    
    public static class TicketResultValidar {
        private STATUS_VALIDACAO status;
        private Aluno aluno;

        public TicketResultValidar(STATUS_VALIDACAO status, Aluno aluno) {
            this.status = status;
            this.aluno = aluno;
        }

        // Getters e setters
        public STATUS_VALIDACAO getStatus() {
            return status;
        }

        public void setStatus(STATUS_VALIDACAO status) {
            this.status = status;
        }

        public Aluno getAluno() {
            return aluno;
        }

        public void setAluno(Aluno aluno) {
            this.aluno = aluno;
        }
    }

    public TicketService(String addressDataBase) {
        daoAluno = new DaoAluno(addressDataBase);
        daoTicket = new DaoTicket(addressDataBase);
        daoHorario = new DaoHorario(addressDataBase);
        daoRefeicao = new DaoRefeicao(addressDataBase);
        daoCurso = new DaoCurso(addressDataBase);
        daoHistorico = new DaoHistorico(addressDataBase);
    }

    private Aluno buscaAluno(String prontuarioEntrada) {
        String prontuario = prontuarioEntrada.toLowerCase();
        if (!StringUtils.isNumeric(prontuario.substring(0, 2))) {
            prontuario = prontuario.substring(2, prontuario.length());
        }
        return daoAluno.buscar(prontuario);
    }

    public static enum STATUS_GERAR_TICKET {
        DIA_INVALIDO,
        ALUNO_NAO_ENCONTRADO,
        TICKET_GERADO,
        TICKET_JA_GERADO,
        SENHA_INCORRETA,
        DIA_INDISPONIVEL,
        FORA_HORARIO_SERVICO,
        CURSO_NAO_POSSUI_QUANTITATIVO, //NOVO
        SEM_CREDITO_E_ASSISTENCIAESTUDANTIL
    }

    public static enum STATUS_VALIDACAO {
        ALUNO_NAO_ENCONTRADO,
        TICKET_LIBERADO,
        TICKET_JA_LIBERADO,
        TICKET_NAO_GERADO,
        SENHA_INCORRETA,
        FORA_HORARIO_SERVICO
    }

    public TicketResultValidar validaTicket(Calendar data, String prontuario, String senha) throws InterruptedException {
        prontuario = prontuario.toLowerCase();
        STATUS_VALIDACAO statusRetorno = null;
        Aluno aluno = null;

        int hora = data.get(Calendar.HOUR_OF_DAY);
        int minuto = data.get(Calendar.MINUTE);

        if (Horarios.isHorarioConsumo(hora, minuto)) {
            // <editor-fold desc="Valida Ticket">
            aluno = buscaAluno(prontuario.toLowerCase());
            //Se aluno foi encontrado
            if (aluno != null) {
                // <editor-fold desc="Ticket encontrado">
                if (daoTicket.buscar(data, aluno.getId())) {
                    // <editor-fold desc="Existe ticket gerado">
                    
                    boolean senhaValida = (senha == null?validaSenha(aluno):validaSenha(aluno, senha));
                    
                    if (senhaValida) {
                        if (daoRefeicao.inserir(data, aluno.getId())) {
                            statusRetorno = STATUS_VALIDACAO.TICKET_LIBERADO;
                        } else {
                            statusRetorno = STATUS_VALIDACAO.TICKET_JA_LIBERADO;
                        }
                    } else {
                        statusRetorno = STATUS_VALIDACAO.SENHA_INCORRETA;
                    }
                    // </editor-fold>
                } else // <editor-fold desc="Ticket não foi gerado">
                {
                    statusRetorno = STATUS_VALIDACAO.TICKET_NAO_GERADO;
                }
                // </editor-fold>
                // </editor-fold>
            } else //Aluno não encontrado
            {
                statusRetorno = STATUS_VALIDACAO.ALUNO_NAO_ENCONTRADO;
            }
            // </editor-fold>
        } else // <editor-fold desc="Fora do horário">
        {
            statusRetorno = STATUS_VALIDACAO.FORA_HORARIO_SERVICO;
        }
        // </editor-fold>

        return new TicketResultValidar(statusRetorno, aluno);
    }

    public TicketResultGerar gerarTicket(Calendar data, String prontuarioEntrada, String senha) throws InterruptedException {
        String prontuario = prontuarioEntrada.toLowerCase();
        STATUS_GERAR_TICKET statusRetorno = null;
        Aluno aluno = null;
        if (Horarios.isHorarioGerarTicket(data)) {
            // <editor-fold desc="Geração de Ticker">

            int diaSemana = data.get(Calendar.DAY_OF_WEEK);
            //Se é sabado ou domingo
            if (diaSemana == 1 || diaSemana == 7) {
                statusRetorno = STATUS_GERAR_TICKET.DIA_INVALIDO;
            } 
            else {
                //Se é de segunda a sexta-feira
                
                // <editor-fold desc="Modificações">
                
                //Busca Aluno
                aluno = buscaAluno(prontuario);

                if (aluno != null) {
                    //Se aluno foi encontrado
                    
                    if (validaDia(aluno, diaSemana)) {
                        //Se é uma data válida
                        boolean senhaValida = (senha == null?validaSenha(aluno):validaSenha(aluno, senha));
                        
                        if (senhaValida) {
                            //Se possui uma senha válida

                            //Busca pela cota do curso do aluno
                            Curso curso = daoCurso.buscaPorId(aluno.getCursoId());
                            Cota cota = curso.getCota();
                                
                            
                            boolean podeGerarTicket = false;
                            boolean isCredito = false;
                            //Se não possui cota, provalvelmente é do ensino médio
                            //Então pode gerar o ticket
                            if (cota == null){
                                podeGerarTicket = true;
                            }else{
                                //Verifica se possui cota disponível
                                
                                //Qual é a data da próxima refeição
                                //Somar as refeições já geradas
                                //Verifica se ainda possui quantitativo
                                int qtdCotaGerada = daoTicket.buscaCotasConsumidas(data, cota);
                                    
                                // Se o curso possui cota disponível
                                if(cota.getQuantidade() > qtdCotaGerada){   
                                     //Se aluno recebe assistência estudantil pode comer
                                    if (aluno.isAssistenciaEstudantil()){
                                        podeGerarTicket = true;
                                    }else if (aluno.getCreditoRefeicao() > 0){
                                        podeGerarTicket = true;
                                        isCredito = true;
                                    }else{
                                        statusRetorno = STATUS_GERAR_TICKET.SEM_CREDITO_E_ASSISTENCIAESTUDANTIL;
                                    }
                                }else{
                                    statusRetorno = STATUS_GERAR_TICKET.CURSO_NAO_POSSUI_QUANTITATIVO;
                                }                                 
                            }
                            
                            if (podeGerarTicket){
                                if (daoTicket.inserir(data, aluno.getId())) {  
                                    statusRetorno = STATUS_GERAR_TICKET.TICKET_GERADO;
                                    if (isCredito)
                                        daoHistorico.IncrementaUnidade(aluno, -1);
                                }else {    
                                    statusRetorno = STATUS_GERAR_TICKET.TICKET_JA_GERADO;    
                                }
                            }
                            
                        } else {
                            statusRetorno = STATUS_GERAR_TICKET.SENHA_INCORRETA;
                        } 
                    } else {
                        statusRetorno = STATUS_GERAR_TICKET.DIA_INDISPONIVEL;
                    }
                } else {
                    statusRetorno = STATUS_GERAR_TICKET.ALUNO_NAO_ENCONTRADO;
                }                
                // </editor-fold>

            }
        } else // <editor-fold desc="Não é horário para gerar ou consumir ticker">
        {
            statusRetorno = STATUS_GERAR_TICKET.FORA_HORARIO_SERVICO;
        }
        
        return new TicketResultGerar(statusRetorno, aluno);
    }

    private boolean validaDia(Aluno aluno, int diaSemana) throws InterruptedException {
        boolean valido = false;

        Horario horario = daoHorario.buscar(aluno);

        if (horario != null && (diaSemana >= 2 && diaSemana <= 6)) {

            if ((diaSemana == 2 && horario.isSeg())
                    || (diaSemana == 3 && horario.isTer())
                    || (diaSemana == 4 && horario.isQua())
                    || (diaSemana == 5 && horario.isQui())
                    || (diaSemana == 6 && horario.isSex())) {
                valido = true;
            }
        }

        return valido;
    }

    private boolean validaSenha(Aluno aluno) {
        if (!aluno.getSenha().trim().equals("")) {
            JPasswordField jpf = new JPasswordField();
            JOptionPane optionPane = new JOptionPane(new Object[]{new JLabel("Digite a senha:"), jpf}, JOptionPane.OK_CANCEL_OPTION) {
                @Override
                public void selectInitialValue() {
                    jpf.requestFocusInWindow();
                }
            };
            optionPane.createDialog(null, "Senha").setVisible(true);
            String senha = new String(jpf.getPassword());
            return validaSenha(aluno, senha);
        }else{
            return true;
        }
    }

    private boolean validaSenha(Aluno aluno, String senha) {
        return aluno.getSenha().equals(senha);
    }
}
