package br.edu.ifsp.sica.business.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

public class DaoRefeicao extends Dao {

    public DaoRefeicao(String addressDataBase) {
        super(addressDataBase);
    }

    public boolean inserir(Calendar data, int idAluno) {
        boolean retorno = false;
        
        int dia, mes, ano;
        
        dia = data.get(Calendar.DAY_OF_MONTH);
        mes = data.get(Calendar.MONTH) + 1;
        ano = data.get(Calendar.YEAR);

        conectar();

        try {
            String dataFormatada = String.format("%02d-%02d-%04d", dia, mes, ano);
            
            PreparedStatement ps = getConn().prepareStatement("select * from relatorio_refeicao where aluno_id = ? and data = to_date(?, 'dd-mm-yyyy')");
            ps.setInt(1, idAluno);
            ps.setString(2, dataFormatada);
            
            ResultSet rs = ps.executeQuery();

            if (!rs.next()) {
                PreparedStatement rs2 = getConn().prepareStatement("insert into relatorio_refeicao(data,aluno_id) values (?,?);");
                rs2.setDate(1, new java.sql.Date(data.getTimeInMillis()));
                rs2.setInt(2, idAluno);
                rs2.execute();
                rs2.close();
                retorno = true;
            } else {
                retorno = false;
            }
            
            rs.close();

        } catch (SQLException e) {
            imprimeErro("Erro ao buscar ticket", e.getMessage());
            retorno = false;
        }
        
        return retorno;
    }
    
    public boolean remove(Calendar data, int idAluno) {
        boolean retorno = false;
        
        int dia, mes, ano;
        
        dia = data.get(Calendar.DAY_OF_MONTH);
        mes = data.get(Calendar.MONTH) + 1;
        ano = data.get(Calendar.YEAR);

        conectar();

        try {
            String dataFormatada = String.format("%02d-%02d-%04d", dia, mes, ano);
            
            PreparedStatement ps = getConn().prepareStatement(
                    "delete from relatorio_refeicao where aluno_id = ? and data = to_date(?, 'dd-mm-yyyy')");
            ps.setInt(1, idAluno);
            ps.setString(2, dataFormatada);
            
            int linhasAfetadas = ps.executeUpdate();

            if (linhasAfetadas > 0) {
                retorno = true;
            }
            
            ps.close();

        } catch (SQLException e) {
            imprimeErro("Erro ao buscar ticket", e.getMessage());
            retorno = false;
        }
        
        return retorno;
    }    
}
