package br.edu.ifsp.sica.business.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import br.edu.ifsp.sica.business.model.Cota;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;

public class DaoCota extends Dao {

    public DaoCota(String addressDataBase) {
        super(addressDataBase);
    }

    public Cota buscar(int id) {
        conectar();
        Cota cota = null;
        
        try {
            PreparedStatement ps = getConn().prepareStatement(
                    " select  id, nome, quantidade"+
                    " from cota_cota"+
                    " where id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cota = new Cota();
                cota.setId(rs.getInt("id"));
                cota.setNome(rs.getString("nome"));
                cota.setQuantidade(rs.getInt("quantidade"));
            }
            rs.close();
            return cota;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar cota", e.getMessage());
            return null;
        }
    }
    
    public Cota buscar(String nome) {
        conectar();
        Cota cota = null;
        
        try {
            PreparedStatement ps = getConn().prepareStatement(
                    " select  id, nome, quantidade"+
                    " from cota_cota"+
                    " where nome = ?");
            ps.setString(1, nome);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cota = new Cota();
                cota.setId(rs.getInt("id"));
                cota.setNome(rs.getString("nome"));
                cota.setQuantidade(rs.getInt("quantidade"));
            }
            rs.close();
            return cota;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar cota", e.getMessage());
            return null;
        }
    }
    
    public boolean insert(Cota cota) {
        conectar();
        boolean retorno = false;
        
        try {
            PreparedStatement ps = getConn().prepareStatement(
                    " insert into cota_cota"
                    + " (nome, quantidade)"
                    + " values"
                    + " (?, ?) ", Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, cota.getNome());
            ps.setInt(2, cota.getQuantidade());
            
            int linhasAfetadas = ps.executeUpdate();
            
             if (linhasAfetadas > 0) {
                ResultSet generatedKeys = ps.getGeneratedKeys();
                if (generatedKeys.next()) {
                    int idGerado = generatedKeys.getInt(1);
                    cota.setId(idGerado);
                    generatedKeys.close();
                }
            }
            ps.close();
            return retorno;
        } catch (SQLException e) {
            imprimeErro("Erro ao inserir cota", e.getMessage());
            return retorno;
        }
    }  
}
