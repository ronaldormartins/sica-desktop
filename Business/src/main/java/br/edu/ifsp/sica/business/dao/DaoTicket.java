package br.edu.ifsp.sica.business.dao;

import br.edu.ifsp.sica.business.model.Cota;
import br.edu.ifsp.sica.utils.Horarios;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;

public class DaoTicket extends Dao {

    public DaoTicket(String addressDataBase) {
        super(addressDataBase);
    }

    public boolean buscar(Calendar dataBase, int idAluno) {
        boolean retorno = false;
        int dia, mes, ano;
        dia = dataBase.get(Calendar.DAY_OF_MONTH);
        mes = dataBase.get(Calendar.MONTH) + 1;
        ano = dataBase.get(Calendar.YEAR);

        conectar();

        try {
            String dataFormatada = String.format("%02d-%02d-%04d", dia, mes, ano);
            
            PreparedStatement ps = getConn().prepareStatement("select * from relatorio_ticket where aluno_id = ? and data = to_date(?, 'dd-mm-yyyy')");
            ps.setInt(1, idAluno);
            ps.setString(2, dataFormatada);
            ResultSet rs = ps.executeQuery();
            retorno = rs.next();
            rs.close();
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar ticket", e.getMessage());
            return false;
        }
        
        return retorno;
    }

    public boolean inserir(Calendar dataBase, int idAluno) {
        boolean retorno = false;
        String data = Horarios.getDataProximaRefeicao(dataBase);
        
        conectar();

        try {
            PreparedStatement ps = 
                    getConn().prepareStatement(
                            "select * from relatorio_ticket " + 
                            "where aluno_id = ? and data = to_date( ?, 'dd/mm/yyyy' )");
            ps.setLong(1, idAluno);
            ps.setString(2, data);
            ResultSet rs = ps.executeQuery();
            if (!rs.next()) {
                PreparedStatement stmt = getConn().
                        prepareStatement("insert into relatorio_ticket(data, aluno_id, tipo) values (to_date( ?, 'dd/mm/yyyy' ),?,?);", 
                        Statement.RETURN_GENERATED_KEYS);
                stmt.setString(1, data);
                stmt.setInt(2, idAluno);
                stmt.setInt(3, 0);
                int linhasAfetadas = stmt.executeUpdate();
            
                if (linhasAfetadas > 0) 
                    retorno = true;
                
                rs.close();
                
            } 

        } catch (SQLException e) {
            imprimeErro("Erro ao buscar ticket", e.getMessage());
            return false;
        }
        
        return retorno;
    }
    
    public boolean remove(Calendar dataBase, int idAluno) {
        boolean retorno = true;
        
        int dia, mes, ano;
        dia = dataBase.get(Calendar.DAY_OF_MONTH);
        mes = dataBase.get(Calendar.MONTH) + 1;
        ano = dataBase.get(Calendar.YEAR);
        
        String dataFormatada = String.format("%02d-%02d-%04d", dia, mes, ano);
        
        conectar();

        try {
            
            PreparedStatement ps = getConn().prepareStatement(
                    "delete from relatorio_ticket "+
                     "where aluno_id = ? and data = to_date( ?, 'dd/mm/yyyy' )");
            ps.setInt(1, idAluno);
            ps.setString(2, dataFormatada);
            
            int linhasAfetadas = ps.executeUpdate();

            if (linhasAfetadas > 0){
                retorno = true;
            }
           
            ps.close();
            
        } catch (SQLException e) {
            imprimeErro("Erro ao remover ticket", e.getMessage());
            return false;
        }
        
        return retorno;
    }
    
    public int buscaCotasConsumidas(Calendar dataBase, Cota cota){
        int qtdCotasConsumidas = 0;
        
        String data = Horarios.getDataProximaRefeicao(dataBase);
        
        conectar();
        
        try {
            PreparedStatement ps = getConn().prepareStatement(
                " select count(*) as qtd"+
                " from relatorio_ticket "+
                " where"+
                "  data = to_date( ?, 'dd/mm/yyyy' ) and "+
                "  aluno_id in "+
                "  ("+
                "    select id from aluno_aluno where cast(curso as integer) in "+
                "       ("+
                "         select id from curso_curso where cota_id = ?"+
                "       )"+
                "  );"
            );
            ps.setString(1, data);
            ps.setInt(2, cota.getId());
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                qtdCotasConsumidas = rs.getInt("qtd");
            }
            rs.close();
            return qtdCotasConsumidas;
        } catch (SQLException e) {
            imprimeErro("Erro ao buscar horario", e.getMessage());
        }
        
        return qtdCotasConsumidas;
    }
}
