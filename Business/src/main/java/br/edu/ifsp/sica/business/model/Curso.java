package br.edu.ifsp.sica.business.model;

public class Curso {
    private int id;
    private String curso;
    private Cota cota;
    private boolean ativo;

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCurso() {
        return curso;
    }

    public int getId() {
        return id;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setCota(Cota cota) {
        this.cota = cota;
    }

    public Cota getCota() {
        return cota;
    }
}
