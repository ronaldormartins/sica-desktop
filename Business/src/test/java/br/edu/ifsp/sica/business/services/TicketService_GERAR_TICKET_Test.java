package br.edu.ifsp.sica.business.services;

import br.edu.ifsp.sica.business.dao.DaoAluno;
import br.edu.ifsp.sica.business.dao.DaoCota;
import br.edu.ifsp.sica.business.dao.DaoCurso;
import br.edu.ifsp.sica.business.dao.DaoHistorico;
import br.edu.ifsp.sica.business.dao.DaoHorario;
import br.edu.ifsp.sica.business.dao.DaoTicket;
import br.edu.ifsp.sica.business.model.Aluno;
import br.edu.ifsp.sica.business.model.Cota;
import br.edu.ifsp.sica.business.model.Curso;
import br.edu.ifsp.sica.business.model.Horario;
import java.util.Calendar;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TicketService_GERAR_TICKET_Test {
    
    private final String addressDatabase = "127.0.0.1";
    
    public TicketService_GERAR_TICKET_Test() {
    }
    
    @Test
    public void DIA_INVALIDO_DOMINGO() throws Exception {
        //Passa uma data (domingo)
        //Não importa o prontuario se a data for inválida
        TicketService ticketService = new TicketService(addressDatabase);
        
        Calendar data = Calendar.getInstance();
        data.set(2023, Calendar.JULY,16,10,0,0);
        String prontuario = "1111111";
        
        TicketService.STATUS_GERAR_TICKET status =  ticketService.gerarTicket(data, prontuario, null).getStatus();
        assertEquals(TicketService.STATUS_GERAR_TICKET.DIA_INVALIDO, status);
    }
    
    @Test
    public void DIA_INVALIDO_SABADO() throws Exception {
        //Passa uma data (Sábado) 
        //Não importa o prontuario se a data for inválida
        TicketService ticketService = new TicketService(addressDatabase);
        
        Calendar data = Calendar.getInstance();
        data.set(2023, Calendar.JULY,15,10,0,0);
        String prontuario = "1111111";
        
        TicketService.STATUS_GERAR_TICKET status =  ticketService.gerarTicket(data, prontuario, null).getStatus();
        assertEquals(TicketService.STATUS_GERAR_TICKET.DIA_INVALIDO, status);
    }
    
    
    @Test
    public void ALUNO_NAO_ENCONTRADO() throws Exception {
        Calendar data = Calendar.getInstance();
        data.set(2023, Calendar.JULY,17,10,0,0);
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar("1111111");
        if (aluno != null){
            DaoTicket daoTicket = new DaoTicket(addressDatabase);
            DaoHorario daoHorario = new DaoHorario(addressDatabase);
            daoTicket.remove(data, aluno.getId());
            daoHorario.remove(aluno);
            daoAluno.remove(aluno);
        }
        
        //Informa um prontuario que não existe
        TicketService ticketService = new TicketService(addressDatabase);
        
        String prontuario = "1111111";
        
        TicketService.STATUS_GERAR_TICKET status =  ticketService.gerarTicket(data, prontuario, null).getStatus();
        assertEquals(TicketService.STATUS_GERAR_TICKET.ALUNO_NAO_ENCONTRADO, status);
    }
    
    @Test
    public void SENHA_INCORRETA() throws Exception {
        String prontuario = "5555555";
        
        TicketService ticketService = new TicketService(addressDatabase);
        
        Calendar data = Calendar.getInstance();
        data.set(2023, Calendar.JULY,17,9,0,0);
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno == null){
            aluno = new Aluno();
            aluno.setAno(1);
            aluno.setAtivo(true);
            aluno.setCursoId(1);
            aluno.setNome("Teste");
            aluno.setSobrenome("Senha Incorreta");
            aluno.setSenha("4321");
            aluno.setProntuario(prontuario);
            daoAluno.insert(aluno);
        }
        
        DaoHorario daoHorario = new DaoHorario(addressDatabase);
        Horario horario = daoHorario.buscar(aluno);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno);
            horario.setSeg(true);
            horario.setTer(true);
            horario.setQua(true);
            horario.setQui(true);
            horario.setSex(true);
            daoHorario.insert(aluno, horario);
        }
        
        
        TicketService.STATUS_GERAR_TICKET status =  ticketService.gerarTicket(data, prontuario,"123").getStatus();
        assertEquals(TicketService.STATUS_GERAR_TICKET.SENHA_INCORRETA, status);
    }
    
    
    @Test
    public void DIA_INDISPONIVEL_HORARIONULO() throws Exception {
        String prontuario = "2222222";
        
        TicketService ticketService = new TicketService(addressDatabase);
        
        Calendar data = Calendar.getInstance();
        data.set(2023, Calendar.JULY,17,10,0,0);
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno == null){
            aluno = new Aluno();
            aluno.setAno(1);
            aluno.setAtivo(true);
            aluno.setCursoId(1);
            aluno.setNome("Teste");
            aluno.setSobrenome("Horário Nulo");
            aluno.setProntuario(prontuario);
            daoAluno.insert(aluno);
        }
        
        TicketService.STATUS_GERAR_TICKET status =  ticketService.gerarTicket(data, prontuario,null).getStatus();
        assertEquals(TicketService.STATUS_GERAR_TICKET.DIA_INDISPONIVEL, status);
    }
    
    
    @Test
    public void DIA_INDISPONIVEL_HORARIONAOCADASTRADO() throws Exception {
        String prontuario = "3333333";
        
        TicketService ticketService = new TicketService(addressDatabase);
        
        Calendar data = Calendar.getInstance();
        data.set(2023, Calendar.JULY,17,10,0,0);
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno == null){
            aluno = new Aluno();
            aluno.setAno(1);
            aluno.setAtivo(true);
            aluno.setCursoId(1);
            aluno.setNome("Teste");
            aluno.setSobrenome("Horário não cadastrado");
            aluno.setProntuario(prontuario);
            daoAluno.insert(aluno);
        }
        
        DaoHorario daoHorario = new DaoHorario(addressDatabase);
        Horario horario = daoHorario.buscar(aluno);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno);
            horario.setSeg(false);
            horario.setTer(false);
            horario.setQua(false);
            horario.setQui(false);
            horario.setSex(false);
            daoHorario.insert(aluno, horario);
        }
        
        
        TicketService.STATUS_GERAR_TICKET status =  ticketService.gerarTicket(data, prontuario, null).getStatus();
        assertEquals(TicketService.STATUS_GERAR_TICKET.DIA_INDISPONIVEL, status);
    }   

    
    @Test
    public void FORA_HORARIO_SERVICO() throws Exception {
        //Passa uma data é que sábado oou domingo
        String prontuario = "4444444";
        
        TicketService ticketService = new TicketService(addressDatabase);
        
        Calendar data = Calendar.getInstance();
        data.set(2023, Calendar.JULY,16,15,0,0);
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno == null){
            aluno = new Aluno();
            aluno.setAno(1);
            aluno.setAtivo(true);
            aluno.setCursoId(1);
            aluno.setNome("Teste");
            aluno.setSobrenome("Fora horário Serviço");
            aluno.setProntuario(prontuario);
            daoAluno.insert(aluno);
        }
        
        DaoHorario daoHorario = new DaoHorario(addressDatabase);
        Horario horario = daoHorario.buscar(aluno);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno);
            horario.setSeg(true);
            horario.setTer(true);
            horario.setQua(true);
            horario.setQui(true);
            horario.setSex(true);
            daoHorario.insert(aluno, horario);
        }
        
        TicketService.STATUS_GERAR_TICKET status =  ticketService.gerarTicket(data, prontuario, null).getStatus();
        assertEquals(TicketService.STATUS_GERAR_TICKET.FORA_HORARIO_SERVICO, status);
    }
    
    @Test
    public void TICKET_GERADO_SEM_COTA() throws InterruptedException{
        String prontuario1 = "7897897";
        String prontuario2 = "1818181";
        
        Calendar dataGerar = Calendar.getInstance();
        dataGerar.set(2023, Calendar.JULY,17,9,0,0);
        
        Calendar dataRemover = Calendar.getInstance();
        dataRemover.set(2023, Calendar.JULY,18,9,0,0);
        
        TicketService ticketService = new TicketService(addressDatabase);
        
        //Cria Cota se não existe
            //Atribui apenas 1 cota
        DaoCota daoCota = new DaoCota(addressDatabase);
        Cota cota = daoCota.buscar("TEST TICKET GERADO SEM COTA");
        if (cota == null){
            cota = new Cota();
            cota.setNome("TEST TICKET GERADO SEM COTA");
            cota.setQuantidade(1);
            daoCota.insert(cota);
        }
        
        //Cria Curso se não existe
        DaoCurso daoCurso = new DaoCurso(addressDatabase);
        Curso curso = daoCurso.buscaPorNome("CURSO TEST TICKET GERADO S COTA");
        if (curso == null){
            curso = new Curso();
            curso.setCurso("CURSO TEST TICKET GERADO S COTA");
            curso.setAtivo(true);
            curso.setCota(cota);
            daoCurso.insert(curso);
        }
        
        //Cria aluno 1 se não existe a associa qota
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno1 = daoAluno.buscar(prontuario1);
        if (aluno1 == null){
            aluno1 = new Aluno();
            aluno1.setCursoId(curso.getId());
            aluno1.setNome("Teste");
            aluno1.setProntuario(prontuario1);
            aluno1.setSobrenome("Ticket Gerado sem cota");
            aluno1.setAssistenciaEstudantil(true);
            daoAluno.insert(aluno1);
        }
        
        //Verificar se possui horário e se não existe inserir um
        DaoHorario daoHorario = new DaoHorario(addressDatabase);
        Horario horario = daoHorario.buscar(aluno1);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno1);
            horario.setSeg(true);
            horario.setTer(true);
            horario.setQua(true);
            horario.setQui(true);
            horario.setSex(true);
            daoHorario.insert(aluno1, horario);
        }
        
        //Cria aluno 2 se não existe a associa qota
        Aluno aluno2 = daoAluno.buscar(prontuario2);
        if (aluno2 == null){
            aluno2 = new Aluno();
            aluno2.setCursoId(curso.getId());
            aluno2.setNome("Teste");
            aluno2.setProntuario(prontuario2);
            aluno2.setSobrenome("Ticket Gerado sem cota");
            aluno2.setAssistenciaEstudantil(true);
            daoAluno.insert(aluno2);
        }
        
        //Verificar se possui horário e se não existe inserir um
        daoHorario = new DaoHorario(addressDatabase);
        horario = daoHorario.buscar(aluno2);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno2);
            horario.setSeg(true);
            horario.setTer(true);
            horario.setQua(true);
            horario.setQui(true);
            horario.setSex(true);
            daoHorario.insert(aluno2, horario);
        }
        
        DaoTicket daoTicket = new DaoTicket(addressDatabase);
        
        daoTicket.remove(dataRemover, aluno1.getId());
        daoTicket.remove(dataRemover, aluno2.getId());
        
        
        //Gera Ticker para aluno 1
        ticketService.gerarTicket(dataGerar, prontuario1, null);
        
        //Gera Ticker para aluno 2
        TicketService.STATUS_GERAR_TICKET status2 =  ticketService.gerarTicket(dataGerar, prontuario2, null).getStatus();
        
        //Efetua assertEquals
        assertEquals(TicketService.STATUS_GERAR_TICKET.CURSO_NAO_POSSUI_QUANTITATIVO, status2);   
    }
    
    @Test
    public void TICKET_GERADO_ENSINO_MEDIO() throws Exception {
        String prontuario = "8123554";
        Calendar dataGerar = Calendar.getInstance();
        dataGerar.set(2023, Calendar.JULY,17,9,0,0);
        
        Calendar dataRemover = Calendar.getInstance();
        dataRemover.set(2023, Calendar.JULY,18,9,0,0);
        
        //Cria Curso se não existe
        DaoCurso daoCurso = new DaoCurso(addressDatabase);
        Curso curso = daoCurso.buscaPorNome("CURSO TEST TICKET GERADO E. MEDIO");
        if (curso == null){
            curso = new Curso();
            curso.setCurso("CURSO TEST TICKET GERADO E. MEDIO");
            curso.setAtivo(true);
            daoCurso.insert(curso);
        }
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno == null){
            aluno = new Aluno();
            aluno.setCursoId(curso.getId());
            aluno.setNome("Teste");
            aluno.setProntuario(prontuario);
            aluno.setSobrenome("Ticket Gerado Ensino Médio");
            daoAluno.insert(aluno);
        }
        DaoTicket daoTicket = new DaoTicket(addressDatabase);
        daoTicket.remove(dataRemover, aluno.getId());
        
        //Verificar se possui horário e se não existe inserir um
        DaoHorario daoHorario = new DaoHorario(addressDatabase);
        Horario horario = daoHorario.buscar(aluno);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno);
            horario.setSeg(true);
            horario.setTer(true);
            horario.setQua(true);
            horario.setQui(true);
            horario.setSex(true);
            daoHorario.insert(aluno, horario);
        }
        
        TicketService ticketService = new TicketService(addressDatabase);
        TicketService.STATUS_GERAR_TICKET status =  ticketService.gerarTicket(dataGerar, prontuario,null).getStatus();
        assertEquals(TicketService.STATUS_GERAR_TICKET.TICKET_GERADO, status);
    }
    
    @Test
    public void TICKET_GERADO_CREDITO() throws InterruptedException{
        String prontuario = "2626262";
        
        Calendar dataGerar = Calendar.getInstance();
        dataGerar.set(2023, Calendar.JULY,17,9,0,0);
        
        Calendar dataRemover = Calendar.getInstance();
        dataRemover.set(2023, Calendar.JULY,18,9,0,0);
        
        TicketService ticketService = new TicketService(addressDatabase);
        
        //Cria Cota se não existe
            //Atribui apenas 1 cota
        DaoCota daoCota = new DaoCota(addressDatabase);
        Cota cota = daoCota.buscar("TEST TICKET GERADO CRÉDITO");
        if (cota == null){
            cota = new Cota();
            cota.setNome("TEST TICKET GERADO CRÉDITO");
            cota.setQuantidade(1);
            daoCota.insert(cota);
        }
        
        //Cria Curso se não existe
        DaoCurso daoCurso = new DaoCurso(addressDatabase);
        Curso curso = daoCurso.buscaPorNome("CURSO TEST TICKET CRÉDITO");
        if (curso == null){
            curso = new Curso();
            curso.setCurso("CURSO TEST TICKET CRÉDITO");
            curso.setAtivo(true);
            curso.setCota(cota);
            daoCurso.insert(curso);
        }
        
        //Cria aluno 1 se não existe a associa qota
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno == null){
            aluno = new Aluno();
            aluno.setCursoId(curso.getId());
            aluno.setNome("Teste");
            aluno.setProntuario(prontuario);
            aluno.setSobrenome("Ticket Gerado credito");
            daoAluno.insert(aluno);
        }
        
        DaoHistorico daoHistorico = new DaoHistorico(addressDatabase);
        daoHistorico.resetaUnidade(aluno);
        
        //Verificar se possui horário e se não existe inserir um
        DaoHorario daoHorario = new DaoHorario(addressDatabase);
        Horario horario = daoHorario.buscar(aluno);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno);
            horario.setSeg(true);
            horario.setTer(true);
            horario.setQua(true);
            horario.setQui(true);
            horario.setSex(true);
            daoHorario.insert(aluno, horario);
        }
        
        daoHistorico.IncrementaUnidade(aluno, 2);
               
        DaoTicket daoTicket = new DaoTicket(addressDatabase);
        daoTicket.remove(dataRemover, aluno.getId());
        
        //Gera Ticker para aluno 2
        TicketService.STATUS_GERAR_TICKET status =  ticketService.gerarTicket(dataGerar, prontuario, null).getStatus();
        boolean creditoSobra = daoAluno.buscar(aluno.getProntuario()).getCreditoRefeicao() == 1;
        
        //Efetua assertEquals
        assertTrue(TicketService.STATUS_GERAR_TICKET.TICKET_GERADO == status && creditoSobra);
    }
    
    @Test
    public void TICKET_JA_GERADO() throws Exception {
        String prontuario = "8859663";
        Calendar dataGerar = Calendar.getInstance();
        dataGerar.set(2023, Calendar.JULY,17,9,0,0);
        
        Calendar dataRemover = Calendar.getInstance();
        dataRemover.set(2023, Calendar.JULY,18,9,0,0);
        
        //Cria Curso se não existe
        DaoCurso daoCurso = new DaoCurso(addressDatabase);
        Curso curso = daoCurso.buscaPorNome("CURSO TEST TICKET GERADO E. MEDIO");
        if (curso == null){
            curso = new Curso();
            curso.setCurso("CURSO TEST TICKET GERADO E. MEDIO");
            curso.setAtivo(true);
            daoCurso.insert(curso);
        }
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno == null){
            aluno = new Aluno();
            aluno.setCursoId(curso.getId());
            aluno.setNome("Teste");
            aluno.setProntuario(prontuario);
            aluno.setSobrenome("Ticket Gerado Ensino Médio");
            daoAluno.insert(aluno);
        }
        DaoTicket daoTicket = new DaoTicket(addressDatabase);
        daoTicket.remove(dataRemover, aluno.getId());
        
        //Verificar se possui horário e se não existe inserir um
        DaoHorario daoHorario = new DaoHorario(addressDatabase);
        Horario horario = daoHorario.buscar(aluno);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno);
            horario.setSeg(true);
            horario.setTer(true);
            horario.setQua(true);
            horario.setQui(true);
            horario.setSex(true);
            daoHorario.insert(aluno, horario);
        }
        
        TicketService ticketService = new TicketService(addressDatabase);
        
        ticketService.gerarTicket(dataGerar, prontuario,null);
        TicketService.STATUS_GERAR_TICKET status =  ticketService.gerarTicket(dataGerar, prontuario,null).getStatus();
        assertEquals(TicketService.STATUS_GERAR_TICKET.TICKET_JA_GERADO, status);
    }
}