package br.edu.ifsp.sica.business.services;

import br.edu.ifsp.sica.business.dao.DaoAluno;
import br.edu.ifsp.sica.business.dao.DaoHorario;
import br.edu.ifsp.sica.business.dao.DaoRefeicao;
import br.edu.ifsp.sica.business.dao.DaoTicket;
import br.edu.ifsp.sica.business.model.Aluno;
import br.edu.ifsp.sica.business.model.Horario;
import java.util.Calendar;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TicketService_VALIDAR_TICKET_Test {
    
    private final String addressDatabase = "127.0.0.1";
    
    public TicketService_VALIDAR_TICKET_Test() {
    }
   
    
    @Test
    public void ALUNO_NAO_ENCONTRADO() throws Exception {
        String prontuario = "1111111";
        
        Calendar data = Calendar.getInstance();
        data.set(2023, Calendar.JULY,17,10,0,0);
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno != null){
            DaoTicket daoTicket = new DaoTicket(addressDatabase);
            DaoHorario daoHorario = new DaoHorario(addressDatabase);
            daoTicket.remove(data, aluno.getId());
            daoHorario.remove(aluno);
            daoAluno.remove(aluno);
        }
        
        TicketService ticketService = new TicketService(addressDatabase);
        TicketService.STATUS_VALIDACAO status =  ticketService.validaTicket(data, prontuario, null).getStatus();  
    }
    
    
    @Test
    public void TICKET_LIBERADO() throws Exception {
        String prontuario = "1111119";
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno == null){
            aluno = new Aluno();
            aluno.setAno(1);
            aluno.setAtivo(true);
            aluno.setCursoId(1);
            aluno.setNome("Teste");
            aluno.setSobrenome("Ticker Liberado");
            aluno.setProntuario(prontuario);
            daoAluno.insert(aluno);
        }
        
        DaoHorario daoHorario = new DaoHorario(addressDatabase);
        Horario horario = daoHorario.buscar(aluno);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno);
            horario.setSeg(true);
            horario.setTer(true);
            horario.setQua(true);
            horario.setQui(true);
            horario.setSex(true);
            daoHorario.insert(aluno, horario);
        }
        
        Calendar dataGeraTicket = Calendar.getInstance();
        dataGeraTicket.set(2023, Calendar.JULY,17,9,0,0);
        
        Calendar dataRemoveTicket = Calendar.getInstance();
        dataRemoveTicket.set(2023, Calendar.JULY,18,9,0,0);
        
        DaoTicket daoTicket = new DaoTicket(addressDatabase);
        daoTicket.remove(dataRemoveTicket, aluno.getId());
        
        TicketService ticketService = new TicketService(addressDatabase);
        
        ticketService.gerarTicket(dataGeraTicket, prontuario, null);
        
        Calendar dataValidaTicket = Calendar.getInstance();
        dataValidaTicket.set(2023, Calendar.JULY,18,12,1,0);
        
        DaoRefeicao daoRefeicao = new DaoRefeicao(addressDatabase);
        daoRefeicao.remove(dataValidaTicket, aluno.getId());
        
        TicketService.STATUS_VALIDACAO statusValidaTicket = 
            ticketService.validaTicket(dataValidaTicket, prontuario, null).getStatus();
        
        assertEquals(TicketService.STATUS_VALIDACAO.TICKET_LIBERADO, statusValidaTicket);
    }
    
    
    
    @Test
    public void TICKET_JA_LIBERADO() throws Exception {
        
        String prontuario = "2222229";
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno == null){
            aluno = new Aluno();
            aluno.setAno(1);
            aluno.setAtivo(true);
            aluno.setCursoId(1);
            aluno.setNome("Teste");
            aluno.setSobrenome("Ticker Liberado");
            aluno.setProntuario(prontuario);
            daoAluno.insert(aluno);
        }
        
        DaoHorario daoHorario = new DaoHorario(addressDatabase);
        Horario horario = daoHorario.buscar(aluno);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno);
            horario.setSeg(true);
            horario.setTer(true);
            horario.setQua(true);
            horario.setQui(true);
            horario.setSex(true);
            daoHorario.insert(aluno, horario);
        }
        
        Calendar dataGeraTicket = Calendar.getInstance();
        dataGeraTicket.set(2023, Calendar.JULY,17,9,0,0);
        DaoTicket daoTicket = new DaoTicket(addressDatabase);
        daoTicket.remove(dataGeraTicket, aluno.getId());
        
        TicketService ticketService = new TicketService(addressDatabase);
        ticketService.gerarTicket(dataGeraTicket, prontuario, null);
        
        Calendar dataValidaTicket = Calendar.getInstance();
        dataValidaTicket.set(2023, Calendar.JULY,18,12,1,0);
        ticketService.validaTicket(dataValidaTicket, prontuario, null);
        TicketService.STATUS_VALIDACAO statusValidaTicket = 
            ticketService.validaTicket(dataValidaTicket, prontuario, null).getStatus();
        
        assertEquals(TicketService.STATUS_VALIDACAO.TICKET_JA_LIBERADO, statusValidaTicket);
    }
    
    
    
    @Test
    public void TICKET_NAO_GERADO() throws Exception {
        String prontuario = "3333339";
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno == null){
            aluno = new Aluno();
            aluno.setAno(1);
            aluno.setAtivo(true);
            aluno.setCursoId(1);
            aluno.setNome("Teste");
            aluno.setSobrenome("Ticker Liberado");
            aluno.setProntuario(prontuario);
            daoAluno.insert(aluno);
        }
        
        DaoHorario daoHorario = new DaoHorario(addressDatabase);
        Horario horario = daoHorario.buscar(aluno);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno);
            horario.setSeg(true);
            horario.setTer(true);
            horario.setQua(true);
            horario.setQui(true);
            horario.setSex(true);
            daoHorario.insert(aluno, horario);
        }
        
        TicketService ticketService = new TicketService(addressDatabase);
        
        Calendar dataValidaTicket = Calendar.getInstance();
        dataValidaTicket.set(2023, Calendar.JULY,18,12,1,0);
        
        TicketService.STATUS_VALIDACAO statusValidaTicket = 
            ticketService.validaTicket(dataValidaTicket, prontuario, null).getStatus();
        
        assertEquals(TicketService.STATUS_VALIDACAO.TICKET_NAO_GERADO, statusValidaTicket);
    }
    

    
    @Test
    public void SENHA_INCORRETA() throws Exception {
        String prontuario = "4444449";
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno == null){
            aluno = new Aluno();
            aluno.setAno(1);
            aluno.setAtivo(true);
            aluno.setCursoId(1);
            aluno.setNome("Teste");
            aluno.setSobrenome("Ticker Liberado");
            aluno.setProntuario(prontuario);
            aluno.setSenha("1234");
            daoAluno.insert(aluno);
        }
        
        DaoHorario daoHorario = new DaoHorario(addressDatabase);
        Horario horario = daoHorario.buscar(aluno);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno);
            horario.setSeg(true);
            horario.setTer(true);
            horario.setQua(true);
            horario.setQui(true);
            horario.setSex(true);
            daoHorario.insert(aluno, horario);
        }
        
        Calendar dataGeraTicket = Calendar.getInstance();
        dataGeraTicket.set(2023, Calendar.JULY,17,9,0,0);
        DaoTicket daoTicket = new DaoTicket(addressDatabase);
        daoTicket.remove(dataGeraTicket, aluno.getId());
        
        TicketService ticketService = new TicketService(addressDatabase);
        ticketService.gerarTicket(dataGeraTicket, prontuario, "1234");
        
        Calendar dataValidaTicket = Calendar.getInstance();
        dataValidaTicket.set(2023, Calendar.JULY,18,12,1,0);
        
        TicketService.STATUS_VALIDACAO statusValidaTicket = 
            ticketService.validaTicket(dataValidaTicket, prontuario, "4321").getStatus();
        
        assertEquals(TicketService.STATUS_VALIDACAO.SENHA_INCORRETA, statusValidaTicket);
    }
    

    
    
    @Test
    public void FORA_HORARIO_SERVICO() throws Exception {
        String prontuario = "5555559";
        
        DaoAluno daoAluno = new DaoAluno(addressDatabase);
        Aluno aluno = daoAluno.buscar(prontuario);
        if (aluno == null){
            aluno = new Aluno();
            aluno.setAno(1);
            aluno.setAtivo(true);
            aluno.setCursoId(1);
            aluno.setNome("Teste");
            aluno.setSobrenome("Ticker Liberado");
            aluno.setProntuario(prontuario);
            daoAluno.insert(aluno);
        }
        
        DaoHorario daoHorario = new DaoHorario(addressDatabase);
        Horario horario = daoHorario.buscar(aluno);
        if (horario == null){
            horario = new Horario();
            horario.setAluno(aluno);
            horario.setSeg(true);
            horario.setTer(true);
            horario.setQua(true);
            horario.setQui(true);
            horario.setSex(true);
            daoHorario.insert(aluno, horario);
        }
        
        Calendar dataGeraTicket = Calendar.getInstance();
        dataGeraTicket.set(2023, Calendar.JULY,17,9,0,0);
        DaoTicket daoTicket = new DaoTicket(addressDatabase);
        daoTicket.remove(dataGeraTicket, aluno.getId());
        
        TicketService ticketService = new TicketService(addressDatabase);
        

        ticketService.gerarTicket(dataGeraTicket, prontuario, null);
        
        Calendar dataValidaTicket = Calendar.getInstance();
        dataValidaTicket.set(2023, Calendar.JULY,18,9,1,0);
        TicketService.STATUS_VALIDACAO statusValidaTicket = 
            ticketService.validaTicket(dataValidaTicket, prontuario, null).getStatus();
        
        assertEquals(TicketService.STATUS_VALIDACAO.FORA_HORARIO_SERVICO, statusValidaTicket);
    }
}
