package br.edu.ifsp.sica.sicaconsumo.view;

import br.edu.ifsp.sica.business.dao.DaoCardapio;
import br.edu.ifsp.sica.business.model.Aluno;
import br.edu.ifsp.sica.business.model.Cardapio;
import br.edu.ifsp.sica.business.services.TicketService;
import br.edu.ifsp.sica.utils.ConfigManager;
import br.edu.ifsp.sica.utils.Horarios;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Calendar;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class Principal extends javax.swing.JPanel {

    private final TicketService ticketService;
    
    public Principal() {
        System.out.println("Arquivo de configuração: "+ConfigManager.getPathFile());
        
        String addressDatabase = ConfigManager.getConfigValue(ConfigManager.ADDRESS_DATABASE);
        if (addressDatabase == null){
            addressDatabase = "127.0.0.1";
            ConfigManager.setConfigValue(ConfigManager.ADDRESS_DATABASE, addressDatabase);
        }
        
        ticketService = new TicketService(addressDatabase);
        DaoCardapio daoCardapio = new DaoCardapio(addressDatabase);
        Cardapio card = daoCardapio.buscarCardapioProximoTicket(Calendar.getInstance());

        initComponents();

        if (card.getPrincipal() == null) {
            lblTxPrato.setText("<html><body><div style='padding: 10px; font-size: 14px;'><p style='padding: 5px;'>" + "Nenhum Registro para o Dia" + "</p></div></body></html>");
        } else {
            lblTxPrato.setText("<html><body><div style='padding: 10px; font-size: 14px;'><p style='padding: 5px;'>" + card.getPrincipal().replace(";", "</p><p style='padding: 5px;'>") + "</p></div></body></html>");
        }
        if (card.getSalada() == null) {
            lblTxSalada.setText("<html><body><div style='padding: 10px; font-size: 14px;'>" + "Nenhum Registro para o Dia" + "</div></body></html>");
        } else {
            lblTxSalada.setText("<html><body><div style='padding: 10px; font-size: 14px;'>" + card.getSalada() + "</div></body></html>");
        }
        if (card.getSobremesa() == null) {
            lblTxSobremesa.setText("<html><body><div style='padding: 10px; font-size: 14px;'>" + "Nenhum Registro para o Dia" + "</div></body></html>");
        } else {
            lblTxSobremesa.setText("<html><body><div style='padding: 10px; font-size: 14px;'>" + card.getSobremesa() + "</div></body></html>");
        }
        if (card.getSobremesa() == null) {
            lblTxSuco.setText("<html><body><div style='padding: 10px; font-size: 14px;'>" + "Nenhum Registro para o Dia" + "</div></body></html>");
        } else {
            lblTxSuco.setText("<html><body><div style='padding: 10px; font-size: 14px;'>" + card.getSuco() + "</div></body></html>");
        }
        
        lblData.setText("Data: "+Horarios.getDataProximaRefeicao(Calendar.getInstance()));
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblProntuario = new javax.swing.JLabel();
        tfProntuario = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        lblNome = new javax.swing.JLabel();
        lblSobrenome = new javax.swing.JLabel();
        tfNome = new javax.swing.JTextField();
        tfSobrenome = new javax.swing.JTextField();
        lblCurso = new javax.swing.JLabel();
        tfCurso = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        lblStatus = new javax.swing.JLabel();
        panelCardapio = new javax.swing.JPanel();
        lblPrato = new javax.swing.JLabel();
        lblTxPrato = new javax.swing.JLabel();
        lblSalada = new javax.swing.JLabel();
        lblTxSalada = new javax.swing.JLabel();
        lblSobremesa = new javax.swing.JLabel();
        lblTxSobremesa = new javax.swing.JLabel();
        lblSuco = new javax.swing.JLabel();
        lblTxSuco = new javax.swing.JLabel();
        lblData = new javax.swing.JLabel();

        lblProntuario.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblProntuario.setText("Prontuário:");

        tfProntuario.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        tfProntuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfProntuarioActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Dados do Aluno:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 14))); // NOI18N

        lblNome.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblNome.setText("Nome:");

        lblSobrenome.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblSobrenome.setText("Sobrenome:");

        tfNome.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        tfNome.setEnabled(false);

        tfSobrenome.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        tfSobrenome.setEnabled(false);

        lblCurso.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblCurso.setText("Curso:");

        tfCurso.setEditable(false);
        tfCurso.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        tfCurso.setEnabled(false);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Status", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 14))); // NOI18N

        lblStatus.setFont(new java.awt.Font("Noto Sans", 1, 42)); // NOI18N
        lblStatus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblStatus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblNome)
                        .addGap(49, 49, 49)
                        .addComponent(tfNome, javax.swing.GroupLayout.DEFAULT_SIZE, 432, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblCurso)
                        .addGap(49, 49, 49)
                        .addComponent(tfCurso))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblSobrenome)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfSobrenome)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNome)
                    .addComponent(tfNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSobrenome)
                    .addComponent(tfSobrenome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCurso))
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelCardapio.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cardapio:", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 18))); // NOI18N

        lblPrato.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblPrato.setText("Prato Principal:");

        lblTxPrato.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lblSalada.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblSalada.setText("Salada:");

        lblTxSalada.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lblSobremesa.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblSobremesa.setText("Sobremesa:");

        lblTxSobremesa.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lblSuco.setFont(new java.awt.Font("Noto Sans", 0, 14)); // NOI18N
        lblSuco.setText("Suco:");

        lblTxSuco.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lblData.setFont(new java.awt.Font("Noto Sans", 0, 18)); // NOI18N
        lblData.setText("Data:");

        javax.swing.GroupLayout panelCardapioLayout = new javax.swing.GroupLayout(panelCardapio);
        panelCardapio.setLayout(panelCardapioLayout);
        panelCardapioLayout.setHorizontalGroup(
            panelCardapioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCardapioLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCardapioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblTxPrato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTxSalada, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTxSobremesa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTxSuco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelCardapioLayout.createSequentialGroup()
                        .addGroup(panelCardapioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPrato)
                            .addComponent(lblSalada)
                            .addComponent(lblSobremesa)
                            .addComponent(lblSuco)
                            .addComponent(lblData))
                        .addGap(0, 453, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelCardapioLayout.setVerticalGroup(
            panelCardapioLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCardapioLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(lblData)
                .addGap(18, 18, 18)
                .addComponent(lblPrato)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTxPrato, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblSalada)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTxSalada, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSobremesa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTxSobremesa, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblSuco)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblTxSuco, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblProntuario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfProntuario))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelCardapio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblProntuario)
                            .addComponent(tfProntuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(panelCardapio, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    
    private void tfProntuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfProntuarioActionPerformed
        try{
            if (!tfProntuario.getText().equals("")){
                Calendar data = Calendar.getInstance();
                

                TicketService.TicketResultGerar result = ticketService.gerarTicket(data, tfProntuario.getText(), null);
                TicketService.STATUS_GERAR_TICKET status = result.getStatus();
                if (status == TicketService.STATUS_GERAR_TICKET.DIA_INVALIDO || status == TicketService.STATUS_GERAR_TICKET.DIA_INDISPONIVEL)
                    //Dia inválido (sábado oo domingo) ou aluno não possui horário cadastrado
                    message(STATUS.ERROR, "Dia inválido", "Dia inválido", result.getAluno());
                else if (status == TicketService.STATUS_GERAR_TICKET.ALUNO_NAO_ENCONTRADO)
                    //Aluno não encontrado
                    message(STATUS.ERROR, "Não Encontrado", "Não Encontrado", result.getAluno());
                else if (status == TicketService.STATUS_GERAR_TICKET.TICKET_GERADO)
                    message(STATUS.SUCCESS, "Ticket Gerado", "Ticket Gerado", result.getAluno());
                else if (status == TicketService.STATUS_GERAR_TICKET.TICKET_JA_GERADO)
                    message(STATUS.ERROR, "Já Contabilizada", "Já Contabilizada", result.getAluno());
                else if (status == TicketService.STATUS_GERAR_TICKET.SENHA_INCORRETA)
                    message(STATUS.ERROR, "Senha Incorreta", "Senha Incorreta", result.getAluno());
                else if (status == TicketService.STATUS_GERAR_TICKET.FORA_HORARIO_SERVICO)
                    message(STATUS.ERROR, "Fora do horário de serviço", "Fora do horário", result.getAluno());
                else if (status == TicketService.STATUS_GERAR_TICKET.CURSO_NAO_POSSUI_QUANTITATIVO)
                    message(STATUS.ERROR, "Curso não possui cota suficiente", "Curso não possui cota suficiente", result.getAluno());
                else if (status == TicketService.STATUS_GERAR_TICKET.SEM_CREDITO_E_ASSISTENCIAESTUDANTIL)
                    message(STATUS.ERROR, "Não possui crédito ou assistênca estudantil", "Não possui crédito ou assistênca estudantil", result.getAluno());
            }
        }catch(HeadlessException | InterruptedException ex){
            ex.printStackTrace();
            try {
                message(STATUS.ERROR, "Falha!", "Falha!");
            } catch (InterruptedException ex1) {
                ex.printStackTrace();
            }
        } 
    }//GEN-LAST:event_tfProntuarioActionPerformed

    
    private void preencheCampos(Aluno aluno) {
        tfNome.setText(aluno.getNome());
        tfSobrenome.setText(aluno.getSobrenome());
        tfCurso.setText(aluno.getCursoDescricao());
    }

    private void limpaCampos() throws InterruptedException {
        Timer timer = new Timer(3000, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                tfProntuario.setText("");
                tfNome.setText("");
                tfSobrenome.setText("");
                tfCurso.setText("");
                lblStatus.setText("");
            }
        });

        timer.setRepeats(false);
        timer.start();
    }

    
    enum STATUS {
        ERROR,
        SUCCESS,
    }
    
    private void message(STATUS status,String message, String statusMessage, Aluno aluno) throws InterruptedException {
        if (aluno!=null)
            preencheCampos(aluno);
        message(status, message, statusMessage);
    }
    
    private void message(STATUS status,String message, String statusMessage) throws InterruptedException {
        
        if (status == STATUS.SUCCESS)
            lblStatus.setForeground(Color.GREEN);
        else
            lblStatus.setForeground(Color.RED);
        lblStatus.setText(statusMessage);
        
        int messageType = 0;
        if (status == STATUS.SUCCESS)
            messageType = JOptionPane.INFORMATION_MESSAGE;
        else
            messageType = JOptionPane.ERROR_MESSAGE;
        
        JOptionPane MsgBox = new JOptionPane(message, messageType, JOptionPane.CLOSED_OPTION);
        JDialog dlg = MsgBox.createDialog("Aviso");
        dlg.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dlg.addComponentListener(new ComponentAdapter() {
            @Override
            // =====================
            public void componentShown(ComponentEvent e) {
                // =====================
                super.componentShown(e);
                Timer t;
                t = new Timer(2000, new ActionListener() {
                    @Override
                    // =====================
                    public void actionPerformed(ActionEvent e) {
                        // =====================
                        dlg.setVisible(false);
                    }
                });
                t.setRepeats(false);
                t.start();
            }
        });
        dlg.setVisible(true);
        Object n = MsgBox.getValue();
        System.out.println(n);
        System.out.println("Finished");
        dlg.dispose();
        
        limpaCampos();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lblCurso;
    private javax.swing.JLabel lblData;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblPrato;
    private javax.swing.JLabel lblProntuario;
    private javax.swing.JLabel lblSalada;
    private javax.swing.JLabel lblSobremesa;
    private javax.swing.JLabel lblSobrenome;
    private javax.swing.JLabel lblStatus;
    private javax.swing.JLabel lblSuco;
    private javax.swing.JLabel lblTxPrato;
    private javax.swing.JLabel lblTxSalada;
    private javax.swing.JLabel lblTxSobremesa;
    private javax.swing.JLabel lblTxSuco;
    private javax.swing.JPanel panelCardapio;
    private javax.swing.JTextField tfCurso;
    private javax.swing.JTextField tfNome;
    private javax.swing.JTextField tfProntuario;
    private javax.swing.JTextField tfSobrenome;
    // End of variables declaration//GEN-END:variables
}
